/*****************************************************************************
 
 test.h : test all modules
 
 Based on: simple_bus_test
 
 *****************************************************************************/

#ifndef test_h
#define test_h

#include <systemc.h>

#include "sb/simple_bus_master_direct.h"
#include "sb/simple_bus.h"
#include "sb/simple_bus_arbiter.h"

#include "stim/stimulus.h"
#include "comm/communication.h"
#include "comm/communication_if.h"

#include "encry/encryption.h"
#include "rotate/rotation.h"
#include "gscale/greyscale.h"
#include "mem/addr.h"
#include "mem/memory.h"
#include "util/stages.h"



class test: public sc_module {


protected:

	/***** presets *****/
	static const int execution_time_ns = 10000;
	static const int num_blocks        = 4;                          // comm, encry, rote, gscale

	static const int pixel_size        = 4;                          // rgba
	static const int width             = 8;                          // max image width
	static const int height            = 8;                          // max image height
	static const int block_size        = (width*height)*pixel_size;  // max image size in memory



public:

	test(sc_module_name name) : C1("C1") {

		// set memory
		mem = new memory("mem", /*start_address*/ 0x0, /*end_address*/ addr::at_position(num_blocks*block_size) -1);
		bind_presets_to_memory();
		printInfo();


		// create instances
		stim       = new stimulus("stimulus" , width, height);

		comm       = new communication("comm", addr::at_position(0*block_size));
		encry      = new encryption("encry"  , addr::at_position(1*block_size));
		rott       = new rotation("rotate"   , addr::at_position(2*block_size));
		gscale     = new greyscale("gscale"  , addr::at_position(3*block_size));

		bus        = new simple_bus("bus");
		arbiter    = new simple_bus_arbiter("arbiter");


		// set clocks
		stim  ->clock(C1);
		comm  ->clock(C1);
		encry ->clock(C1);
		rott  ->clock(C1);
		gscale->clock(C1);
		bus   ->clock(C1);

		// set common bus
		comm  ->bus_port(*bus);
		encry ->bus_port(*bus);
		rott  ->bus_port(*bus);
		gscale->bus_port(*bus);

		// set ports
		stim ->comm_port(*comm);
		bus  ->arbiter_port(*arbiter);
		bus  ->slave_port(*mem);

		// start
		sc_start(execution_time_ns, SC_NS);

		// print result
		mem->printAllData();

	}

	// destructor
	~test() {
		if (stim)       { delete stim;    stim    = 0; }
		if (comm)       { delete comm;    comm    = 0; }
		if (encry)      { delete encry;   encry   = 0; }
		if (rott)       { delete rott;    rott    = 0; }
		if (gscale)     { delete gscale;  gscale  = 0; }
		if (bus)        { delete bus;     bus     = 0; }
		if (mem)        { delete mem;     mem     = 0; }
		if (arbiter)    { delete arbiter; arbiter = 0; }
	}


	void bind_presets_to_memory();
	void printInfo();



private:

	/***** variables *****/
	sc_clock C1;

	communication      *comm;
	encryption         *encry;
	rotation           *rott;
	greyscale          *gscale;

	memory             *mem;
	stimulus           *stim;

	simple_bus         *bus;
	simple_bus_arbiter *arbiter;



};


/**
 * Add the start position of each block according to the preset block_size
 */
inline void test::bind_presets_to_memory() {

	int addr_comm  = addr::at_position(0*block_size);
	int addr_encry = addr::at_position(1*block_size);
	int addr_rott  = addr::at_position(2*block_size);
	int addr_grey  = addr::at_position(3*block_size);
	int stage      = stages::IDLE;

	mem->direct_write(&addr_comm , addr::ORIGINAL_IMG );
	mem->direct_write(&addr_encry, addr::ENCRYPTED_IMG);
	mem->direct_write(&addr_rott , addr::ROTATED_IMG  );
	mem->direct_write(&addr_grey , addr::GREY_IMG     );
	mem->direct_write(&stage     , addr::STAGE        );

}


inline void test::printInfo() {

	printf("\n");
	printf("***************************************************\n");
	printf("***************** EXECUTION INFO ******************\n");
	printf("***************************************************\n");


	cout << setw(20) << left << "* EXECUTION TIME"  << "| " << execution_time_ns << " ns" << endl;
	cout << setw(20) << left << "* MAX IMAGE SIZE"  << "| " << width*height << " (w: " << width << ", h: " << height << ")" << endl;
	cout << setw(20) << left << "* PIXEL SIZE"      << "| " << pixel_size << endl;
	cout << setw(20) << left << "* BLOCK SIZE"      << "| " << width*height*pixel_size << " (" << width << "x" << height << "x" << pixel_size << ")"<< endl;
	cout << setw(20) << left << "* NUM BLOCKS"      << "| " << num_blocks << " (comm, encry, rott, gscale)" << endl << endl;

	cout << setw(20) << left << "* RESERVED MEMORY" << "| " << "0"                             << "-" << addr::at_position(0*block_size) - 1 << endl;
	cout << setw(20) << left << "* COMM   ADDRESS"  << "| " << addr::at_position(0*block_size) << "-" << addr::at_position(1*block_size) - 1 << endl;
	cout << setw(20) << left << "* ENCRY  ADDRESS"  << "| " << addr::at_position(1*block_size) << "-" << addr::at_position(2*block_size) - 1 << endl;
	cout << setw(20) << left << "* ROTATE ADDRESS"  << "| " << addr::at_position(2*block_size) << "-" << addr::at_position(3*block_size) - 1 << endl;
	cout << setw(20) << left << "* GSCALE ADDRESS"  << "| " << addr::at_position(3*block_size) << "-" << addr::at_position(4*block_size) - 1 << endl;

	printf("***************************************************\n\n");

}


#endif
