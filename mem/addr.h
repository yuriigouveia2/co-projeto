/*
 *	addr.h: store addresses of reserved data
 *
 *	Author:
 */

#ifndef addresses_h
#define addresses_h

#include <iostream>
#include <iomanip>
using namespace std;

class addr {

private:
	const static int NUM_RESERVED_ADDRESSES = 8; // positions from 0 to 7


public:

	// reserved addresses
	static const int STAGE           = 0;  // @see util/stages.h
	static const int IMG_WIDTH       = 4;
	static const int IMG_HEIGHT      = 8;

	static const int ORIGINAL_IMG    = 12;
	static const int ENCRYPTED_IMG   = 16;
	static const int ROTATED_IMG     = 20;
	static const int GREY_IMG        = 24;

	static const int RESERVED_7      = 28;


	// functions
	static int at_position(int decimal_position);
	static void print_reserved_memory(int arr[8]);

};


/**
 * Get a valid address in memory
 * @return: an address which is a multiple value of 4 starting from the the first valid address in memory
 */
inline int addr::at_position(int decimal_position) {

	// fixed
	int word_size = 4;

	int reserved = word_size * NUM_RESERVED_ADDRESSES; // 32
	int position = word_size * decimal_position;


	return reserved + position;

}

inline void addr::print_reserved_memory(int arr[8]) {

	cout << "Pos: 0     | STAGE                  | Value: " << arr[0] << endl;
	cout << "Pos: 1     | IMG_WIDTH              | Value: " << arr[1] << endl;
	cout << "Pos: 2     | IMG_HEIGHT             | Value: " << arr[2] << endl << endl;

	cout << "Pos: 3     | ADDRESS::ORIGINAL_IMG  | Value: " << setw(7) << arr[3] << endl;
	cout << "Pos: 4     | ADDRESS::ENCRYPTED_IMG | Value: " << setw(7) << arr[4] << endl;
	cout << "Pos: 5     | ADDRESS::ROTATED_IMG   | Value: " << setw(7) << arr[5] << endl;
	cout << "Pos: 6     | ADDRESS::GREY_IMG      | Value: " << setw(7) << arr[6] << endl;
	cout << "Pos: 7     | RESERVED_7             | Value: " << setw(7) << arr[7] << endl;

}

#endif
