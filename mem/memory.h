/*****************************************************************************
 
 memory.h : The memory (slave) that contains information about
 addresses in memory_data
 
 Author: Leandro
 Based On: simple_bus_fast_mem.h
 
 *****************************************************************************/

#ifndef memory_h
#define memory_h

#include <systemc.h>
#include <iostream>
#include <iomanip>
using namespace std;

#include "addr.h"
#include "../sb/simple_bus_types.h"
#include "../sb/simple_bus_slave_if.h"



class memory: public simple_bus_slave_if, public sc_module {

public:


	// constructor
	memory(sc_module_name name_, unsigned int start_address, unsigned int end_address) : sc_module(name_), m_start_address(start_address), m_end_address(end_address) {

		sc_assert(m_start_address <= m_end_address);
		sc_assert((m_end_address - m_start_address + 1) % 4 == 0);

		size = (m_end_address - m_start_address + 1) / 4;
		MEM = new int[size];

		for (unsigned int i = 0; i < size; ++i)
			MEM[i] = 0;

	}

	// destructor
	~memory();

	// direct Slave Interface
	bool direct_read(int *data, unsigned int address);
	bool direct_write(int *data, unsigned int address);

	// Slave Interface
	simple_bus_status read(int *data, unsigned int address);
	simple_bus_status write(int *data, unsigned int address);

	unsigned int start_address() const;
	unsigned int end_address() const;

	void printAllData();

private:
	int * MEM;
	unsigned int m_start_address;
	unsigned int m_end_address;
	unsigned int size;

};
// end class memory


inline bool memory::direct_read(int *data, unsigned int address) {
	return (read(data, address) == SIMPLE_BUS_OK);
}


inline bool memory::direct_write(int *data, unsigned int address) {
	return (write(data, address) == SIMPLE_BUS_OK);
}


inline simple_bus_status memory::read(int *data, unsigned int address) {
	*data = MEM[(address - m_start_address) / 4];
	return SIMPLE_BUS_OK;
}


inline simple_bus_status memory::write(int *data, unsigned int address) {
	MEM[(address - m_start_address) / 4] = *data;
	return SIMPLE_BUS_OK;
}


inline memory::~memory() {
	if (MEM)
		delete[] MEM;
	MEM = (int *) 0;
}


inline unsigned int memory::start_address() const {
	return m_start_address;
}


inline unsigned int memory::end_address() const {
	return m_end_address;
}


inline void memory::printAllData() {

	printf("\n\n");
	printf("*****************************************\n");
	printf("************ PRINTING MEMORY ************\n");
	printf("*****************************************\n\n\n");


	printf("************ RESERVED MEMORY ************\n");
	int arr[8] = {MEM[0], MEM[1], MEM[2], MEM[3], MEM[4], MEM[5], MEM[6], MEM[7]};
	addr::print_reserved_memory(arr);


	printf("\n\n************** DATA MEMORY *************\n");

	for (int i = 8; i < size; i++) {

		if (i == MEM[addr::ORIGINAL_IMG /4]/4) printf("*************** COMM DATA **************\n");
		if (i == MEM[addr::ENCRYPTED_IMG/4]/4) printf("************** ENCRY DATA **************\n");
		if (i == MEM[addr::ROTATED_IMG  /4]/4) printf("************** ROTATE DATA *************\n");
		if (i == MEM[addr::GREY_IMG     /4]/4) printf("************** GSCALE DATA *************\n");

		cout << "Pos: ";
		cout << setw(5) << left << i << " | ";
		cout << "Value: " << MEM[i] << endl;

	}

}


#endif
