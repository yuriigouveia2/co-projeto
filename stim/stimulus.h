/*****************************************************************************
 
 communication.h : The communication interface (master) used to receive
 packages and store in memory using the direct BUS interface.
 
 Author: Leandro
 Based On: simple_bus_direct.h, simple_bus_fast_mem.h
 
 *****************************************************************************/

#ifndef __stimulus_h
#define __stimulus_h

#include <systemc>
#include <iomanip>
#include "../comm/communication_if.h"
#include "../util/package.h"
#include "../util/log.h"


class stimulus: public sc_module {

public:

	sc_in_clk clock;
	sc_port<communication_if> comm_port;


	SC_HAS_PROCESS(stimulus);

	stimulus(sc_module_name name_, int width, int height, bool verbose = true) : sc_module(name_), m_verbose(verbose), m_width(width), m_height(height) { // @suppress("Class members should be properly initialized")
		SC_METHOD(main_action);
		sensitive << clock.pos();
	}


	// process
	void main_action();


private:
	bool m_verbose;
	int  m_width;
	int  m_height;

	int i;         // current number of packages sent
	int crc;
	int type;
	int payload[4];

};


inline void stimulus::main_action() {


	// INITIAL PKG
	if (i == 0) {

		type = package::PKG_INITIAL;
		if (m_verbose) log::d(name(), "sending initial package\n");

		payload[0] = m_width;  // width
		payload[1] = m_height; // height
		payload[2] = -1;       // none
		payload[3] = -1;       // none

	}


	// DATA PKG
	else if (i < m_width*m_height + 1) {

		type = package::PKG_DATA;
		if (m_verbose) log::d(name(), "sending data package\n");

		payload[0] = i; // red
		payload[1] = i; // green
		payload[2] = i; // blue
		payload[3] = i; // alplha

	}

	// DONE PKG
	else if (i == m_width*m_height + 1) {

		type = package::PKG_DONE;
		if (m_verbose) log::d(name(), "sending done package\n");

		payload[0] = -1; // none
		payload[1] = -1; // none
		payload[2] = -1; // none
		payload[3] = -1; // none

	}

	// NO MORE JOB
	else
		return;


	// send package
	crc = payload[0] + payload[1] + payload[2] + payload[3];
	package *p = new package(crc, type, payload);
	bool result = comm_port->send_package(p);

	// increment package number
	if (result)
		i++;


}


#endif
