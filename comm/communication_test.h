/*****************************************************************************
 
 communication_test.h : Test the communication module
 
 Based on: simple_bus_test
 
 *****************************************************************************/

#ifndef communication_test_h
#define communication_test_h

#include <systemc.h>

#include "../sb/simple_bus_master_direct.h"
#include "../sb/simple_bus.h"
#include "../sb/simple_bus_arbiter.h"

#include "../stim/stimulus.h"
#include "../mem/memory.h"

#include "communication.h"
#include "communication_if.h"


class communication_test: public sc_module {

public:

	// constructor
	communication_test(sc_module_name name, memory* memory) : C1("C1"), mainMemory(memory) {

		// create instances
		comm       = new communication("comm", 0x20, 10);

		stim       = new stimulus("stimulus", 8, 8);
		bus        = new simple_bus("bus");
		arbiter    = new simple_bus_arbiter("arbiter");


		// connect instances
		comm->clock(C1);
		stim->clock(C1);
		bus ->clock(C1);


		comm->bus_port(*bus);
		stim->comm_port(*comm);
		bus ->arbiter_port(*arbiter);
		bus ->slave_port(*mainMemory);


	}

	// destructor
	~communication_test() {
		if (comm)       { delete comm; comm = 0; }
		if (stim)       { delete stim; stim = 0; }
		if (bus)        { delete bus; bus = 0; }
		if (mainMemory) { delete mainMemory; mainMemory = 0; }
		if (arbiter)    { delete arbiter; arbiter = 0; }
	}

private:

	sc_clock C1;

	communication      *comm;
	stimulus           *stim;
	memory             *mainMemory;

	simple_bus         *bus;
	simple_bus_arbiter *arbiter;

};

#endif
