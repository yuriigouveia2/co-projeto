/*****************************************************************************
 
 communication.cpp : The communication interface (master) used to receive
 packages and store them in memory using the direct BUS interface.
 
 Author: Leandro
 Based On: simple_bus_direct.cpp
 
 *****************************************************************************/

#include "communication.h"

#include "../mem/addr.h"
#include "../sb/simple_bus_types.h"
#include "../util/stages.h"
#include "../util/package.h"
#include "../util/log.h"


void communication::main_action() {
	while (true) {

		while (buffer->empty())
			wait();


		package *pkg = &buffer->front();

		switch (pkg->getType()) {

			case package::PKG_INITIAL:
				stage = stages::RECEIVING_IMG;
				if(m_verbose) log::d(name(), "writing info [stage, w, h] = (%d, %d, %d)\n", stage, *pkg->getImgWidth(), *pkg->getImgHeight());

				bus_port->direct_write(&stage, addr::STAGE);
				bus_port->direct_write(pkg->getImgWidth() , addr::IMG_WIDTH);
				bus_port->direct_write(pkg->getImgHeight(), addr::IMG_HEIGHT);

				wait();
				break;

			case package::PKG_DATA:
				if(m_verbose) log::d(name(), "writing rgba = (%d, %d, %d, %d)\n", *pkg->getRed(), *pkg->getGreen(), *pkg->getBlue(), *pkg->getAlpha());

				bus_port->burst_write(0, pkg->getPayLoad(), m_address, 4, true);
				m_address += 16;
				break;

			case package::PKG_DONE:
				if(m_verbose) log::d(name(), "writing stage = receiving_img_done\n");

				stage = stages::RECEIVING_IMG_DONE;
				bus_port->direct_write(&stage, addr::STAGE);
				wait();

				m_address = 0;
				break;

			default:
				log::e(name(), "Error: package's type not defined");

		}


		buffer->erase(buffer->begin());
		if (buffer->empty()) printf("\n");


	}


}


/**
 * Method called by an external Module (stimulus)
 * @param package: package with crc, type and payload(red, green, blue, alpha)
 */
bool communication::send_package(package *pkg) {

	if ( ! pkg->ckeckCRC() ) {
		log::e(name(), "crc not valid");
		return false;
	}

	buffer->push_back(*pkg);
	return true;

}

