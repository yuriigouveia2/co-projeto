/*****************************************************************************
 
 communication.h : The communication interface (master) used to receive
 packages and store in memory using the direct BUS interface.
 
 Author: Leandro
 Based On: simple_bus_direct.h, simple_bus_fast_mem.h
 
 *****************************************************************************/

#ifndef __communication_h
#define __communication_h

#include <systemc.h>
#include "communication_if.h"
#include "../sb/simple_bus_direct_if.h"
#include "../sb/simple_bus.h"
#include "../util/package.h"



#include <vector>
using namespace std;


class communication: public communication_if, public sc_module {

public:

	sc_in_clk clock;
	sc_port<simple_bus> bus_port;

	SC_HAS_PROCESS(communication);

	communication(sc_module_name name_, unsigned int address, bool verbose = true) : sc_module(name_), m_address(address), m_verbose(verbose) {
		stage = 0;

		buffer = new vector<package>();

		SC_THREAD(main_action);
		sensitive << clock.pos();

	}


	void main_action();

	bool send_package(package *package);

private:
	unsigned int m_address;
	bool m_verbose;

	vector<package> *buffer;
	int stage;
};



#endif
