/*****************************************************************************
 
 communication_if.h :
 
 Author: Leandro
 Based On: simple_bus_direct_if.h
 
 *****************************************************************************/

#ifndef communication_if_h
#define communication_if_h

#include <systemc.h>
#include "../util/package.h"


class communication_if: public virtual sc_interface {

public:

	virtual bool send_package(package *package) = 0;

};


#endif
