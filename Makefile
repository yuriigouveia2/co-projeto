CC=g++

FLAGS = -lsystemc -lm -g -O3 -Wall 

INCLUDES = -I. -I$$SYSTEMC_HOME/include
INCLUDES += -I$$PWD/comm
INCLUDES += -I$$PWD/encry
INCLUDES += -I$$PWD/gscale
INCLUDES += -I$$PWD/mem
INCLUDES += -I$$PWD/rotate
INCLUDES += -I$$PWD/sb
INCLUDES += -I$$PWD/stim
INCLUDES += -I$$PWD/util

LIBS = -L. -L$$SYSTEMC_HOME/lib-linux64 -Wl,-rpath=$$SYSTEMC_HOME/lib-linux64

CPPS = *.cpp
CPPS += comm/*.cpp
CPPS += encry/*.cpp
CPPS += gscale/greyscale.cpp
CPPS += rotate/*.cpp
CPPS += sb/*.cpp

TARGET = run_project

all:
	$(CC) $(FLAGS) $(INCLUDES) $(LIBS) $(CPPS) -o $(TARGET) 

clean:
	rm $(TARGET)



	