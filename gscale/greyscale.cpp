/*****************************************************************************
 
 greyscale.cpp :
 
 Author:
 Based On: simple_bus_direct.cpp
 
 *****************************************************************************/

#include "greyscale.h"

#include "../mem/addr.h"
#include "../util/stages.h"
#include "../util/log.h"


void greyscale::main_action() {
	while (true) {

		// wait for my turn
		while (stage != stages::ROTATING_IMG_DONE) {
			wait();
			bus_port->direct_read(&stage, addr::STAGE);
		}


		// change stage
		stage = stages::CONVERTING_TO_GRAY;
		bus_port->direct_write(&stage, addr::STAGE);
		if (m_verbose) { printf("\n"); log::d(name(), "STARTING CONVERTION TO GREY\n"); }


		// get image dimension
		bus_port->direct_read(&width , addr::IMG_WIDTH);
		bus_port->direct_read(&height, addr::IMG_HEIGHT);
		blocksize = (width*height)*4;


		// get the start address of the previous stage
		int start_address;
		bus_port->direct_read(&start_address, addr::ROTATED_IMG);


		// get all data of the previous stage
		if (m_verbose) { log::d(name(), "getting data from previous stage\n"); }
		buffer = new int[blocksize];
		bus_port->burst_read(/*unique_priority*/ 1, buffer, start_address, blocksize, /*lock*/ true);


		// for (int i = 0; i < size; i++) { log::d(name(), "data from previous stage: (%d)\n", buffer[i]); }


		// main action
		execute_task();


		// change the my stage to done
		stage = stages::CONVERTING_TO_GRAY_DONE;
		bus_port->direct_write(&stage, addr::STAGE);
		if (m_verbose) { log::d(name(), "CONVERTION TO GREY DONE\n"); }

	}

}

void greyscale::execute_task() {


	//*************************************************
	//*************************************************
	//************ WRITE YOUR CODE HERE ***************


	int data[4];
	int value;

	for (int i = 0; i < blocksize; i=i+4) {

		value = buffer[i]*0.3f + buffer[i+1]*0.59f + buffer[i+2]*0.11f;
		
		for(int j = 0; j < 3; j++)
		    data[j] = value;
		data[3] = buffer[i+3];
		
	 	log::d(name(), "writing pixel (%d)\n", i-4);	
	 	log::d(name(), "writing pixel value in R (%d)\n", data[0]);
	 	log::d(name(), "writing pixel value in G (%d)\n", data[1]);
	 	log::d(name(), "writing pixel value in B (%d)\n", data[2]);
	 	log::d(name(), "writing pixel value in A (%d)\n", data[3]);
		bus_port->burst_write(0, data, m_address, 4, true);
	
	// 	bus_port->direct_write(&data[0], m_address);
	// 	bus_port->direct_write(&data[0], m_address+4);
	// 	bus_port->direct_write(&data[0], m_address+8);
	// 	bus_port->direct_write(&buffer[i+3], m_address+12);
		m_address += 16;
	// 	wait();

	}

}
