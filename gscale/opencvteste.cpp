/*compile: g++ opencvteste.cpp -o out `pkg-config opencv --cflags --libs`
  run: ./out           */

#include <opencv2/opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main()
{
	Mat img = imread("lena.png");
	Mat gray = Mat::zeros(img.rows, img.cols, CV_8UC3);
	int B, G, R;

	for(int i = 0; i < img.rows; i++)
	{
		for(int j = 0; j < img.cols; j++)
		{
			B = img.at<Vec3b>(i, j)[0];
			G = img.at<Vec3b>(i, j)[1];
			R = img.at<Vec3b>(i, j)[2];

			gray.at<Vec3b>(i, j)[0] = R*0.3 + G*0.59 + B*0.11;
			gray.at<Vec3b>(i, j)[1] = R*0.3 + G*0.59 + B*0.11;
			gray.at<Vec3b>(i, j)[2] = R*0.3 + G*0.59 + B*0.11;            
		}
	}

	namedWindow("img", WINDOW_NORMAL);
    imshow("img", img);

    namedWindow("g", WINDOW_NORMAL);
    imshow("g", gray);

    waitKey(0);

}