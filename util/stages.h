/*
 *	stages.cpp: possible stages in the application
 *
 *	Author: leandro
 */


class stages {

public:

	const static int IDLE                    = 0;


	const static int RECEIVING_IMG           = 1;
	const static int RECEIVING_IMG_DONE      = 2;

	const static int ENCRYPTING_IMG          = 3;
	const static int ENCRYPTING_IMG_DONE     = 4;

	const static int ROTATING_IMG            = 5;
	const static int ROTATING_IMG_DONE       = 6;

	const static int CONVERTING_TO_GRAY      = 7;
	const static int CONVERTING_TO_GRAY_DONE = 8;


};



