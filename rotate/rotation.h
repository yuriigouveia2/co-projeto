/*****************************************************************************
 
 rotation.h :
 
 Author:
 Based On: simple_bus_direct.h
 
 *****************************************************************************/

#ifndef rotation_h
#define rotation_h

#include <systemc.h>
#include "../sb/simple_bus.h"


class rotation: public sc_module {

public:

	sc_in_clk clock;
	sc_port<simple_bus> bus_port;

	SC_HAS_PROCESS(rotation);

	rotation(sc_module_name name_, unsigned int address, bool verbose = true) : sc_module(name_), m_address(address), m_verbose(verbose), stage(0), width(0), height(0), blocksize(0), buffer(0) {
		SC_THREAD(main_action);
		sensitive << clock.pos();
	}


	void main_action();

	void execute_task();

private:

	unsigned int m_address;
	bool m_verbose;

	int  *buffer;

	int  stage;     // @see util/stages.h
	int  width;     // image width
	int  height;    // image height
	int  blocksize; // width*height*pixel_size

};


#endif
