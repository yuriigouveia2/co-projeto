/*****************************************************************************
 
 rotation.cpp :
 
 Author:
 Based On: simple_bus_direct.cpp
 
 *****************************************************************************/

#include "rotation.h"

#include "../mem/addr.h"
#include "../util/stages.h"
#include "../util/log.h"


void rotation::main_action() {
	while (true) {

		// wait for my turn
		while (stage != stages::ENCRYPTING_IMG_DONE) {
			wait();
			bus_port->direct_read(&stage, addr::STAGE);
		}


		// change stage
		stage = stages::ROTATING_IMG;
		bus_port->direct_write(&stage, addr::STAGE);
		if (m_verbose) { printf("\n"); log::d(name(), "STARTING ENCRYPTING\n"); }


		// get image dimension
		bus_port->direct_read(&width , addr::IMG_WIDTH);
		bus_port->direct_read(&height, addr::IMG_HEIGHT);
		blocksize = (width*height)*4;


		// get the start address of the previous stage
		int start_address;
		bus_port->direct_read(&start_address, addr::ENCRYPTED_IMG);


		// get all data of the previous stage
		if (m_verbose) { log::d(name(), "getting data from previous stage\n"); }
		buffer = new int[blocksize];
		bus_port->burst_read(/*unique_priority*/ 1, buffer, start_address, blocksize, /*lock*/ true);


		// for (int i = 0; i < size; i++) { log::d(name(), "data from previous stage: (%d)\n", buffer[i]); }


		// main action
		execute_task();


		// change the my stage to done
		stage = stages::ROTATING_IMG_DONE;
		bus_port->direct_write(&stage, addr::STAGE);
		if (m_verbose) { log::d(name(), "ROTATING IMG DONE\n"); }

	}

}

void rotation::execute_task() {


	//*************************************************
	//*************************************************
	//************ WRITE YOUR CODE HERE ***************

	// example: multiplying data from previous stage by two

	int data[1];

	for (int i = 0; i < blocksize; i++) {

		data[0] = buffer[i]*2;

		log::d(name(), "writing pixel (%d)\n", i);
		bus_port->direct_write(&data[0], m_address);
		m_address += 4;
		wait();

	}

}
